#!/bin/sh

sed -i -E "s/TO_BE_REPLACED_WITH_PORT/${PORT:-80}/" /etc/nginx/conf.d/*.conf

mkdir /var/tmp/nginx

sed -i -E "s/^;listen.owner = .*/listen.owner = $(whoami)/" /usr/local/etc/php-fpm.d/www.conf

supervisord --nodaemon --configuration /etc/supervisord.conf
