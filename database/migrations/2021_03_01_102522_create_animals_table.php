<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnimalsTable extends Migration
{
    public function up()
    {
        Schema::create('animals', static function (Blueprint $table): void {
            $table->increments('id');
            $table->string('name');
        });
    }

    public function down()
    {
    }
}
