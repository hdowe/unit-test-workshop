FROM php:8.0.2-fpm-alpine3.13

WORKDIR /var/www/html

RUN apk update && apk upgrade \
    && apk add php8-pdo_mysql \
    && apk add nginx \
    && mkdir -p /run/nginx \
    && sed -i -E "s/error_log .+/error_log \/dev\/stderr warn;/" /etc/nginx/nginx.conf \
    && sed -i -E "s/access_log .+/access_log \/dev\/stdout main;/" /etc/nginx/nginx.conf \
    && apk add supervisor \
    && mkdir -p /etc/supervisor.d/ \
    && rm -rf /var/cache/apk/*

RUN docker-php-ext-install pdo_mysql

COPY --from=composer:2.0.9 /usr/bin/composer /usr/bin/composer

ENV NGINX_CONFD_DIR /etc/nginx/conf.d

COPY docker/nginx.conf $NGINX_CONFD_DIR/default.conf
COPY docker/supervisor.programs.ini /etc/supervisor.d/
COPY docker/start.sh /

RUN mkdir -p /var/www/html/storage/framework/cache/data \
    && mkdir -p /var/www/html/storage/framework/views

RUN adduser -D nonroot \
    && chmod a+x /start.sh \
    && chmod -R a+w /etc/nginx \
    && chmod -R a+w /run/nginx \
    && chmod -R a+r /etc/supervisor* \
    && sed -i -E "s/^file=\/run\/supervisord\.sock/file=\/run\/supervisord\/supervisord.conf/" /etc/supervisord.conf \
    && mkdir -p /run/supervisord \
    && chmod -R a+w /run/supervisord \
    && chmod -R a+w /var/log \
    && apk add --update sudo \
    && echo "nonroot ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

COPY . /var/www/html

RUN composer install \
    --prefer-dist \
    --no-dev \
    --no-interaction \
    --ignore-platform-reqs \
    --no-scripts \
    --no-progress \
    --optimize-autoloader

RUN chmod -R a+w /var/www/html

CMD ["/start.sh"]
