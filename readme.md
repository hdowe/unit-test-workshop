# Unit Test Workshop

This repo is intended to be used to illustrate some concepts, ideas and practices surrounding unit testing with PHPUnit.

## Installation

Clone the repo and boot the app.

```shell
docker-compose up -d
```

Then run the install command.

```shell
make install
```
## Usage

The app should be ready! Fire a request to create an animal.

```
POST http://localhost:6060/animals
Accept: application/json
Content-Type: application/json

{
  "name": "Nellie"
}
```

## Tests

To run the unit tests
```shell
make unit-test
```
