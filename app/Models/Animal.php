<?php

declare(strict_types=1);

namespace App\Models;

class Animal
{
    private ?int $id = null;

    private ?string $name = null;

    private ?string $species = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setSpecies(string $species): void
    {
        if (!in_array($species, ['cow', 'sheep'], true)) {
            throw new \InvalidArgumentException('Wrong species');
        }

        $this->species = $species;
    }

    public function getSpecies(): ?string
    {
        return $this->species;
    }
}
