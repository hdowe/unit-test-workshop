<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Models\Animal;

interface AnimalsRepositoryInterface
{
    public function createAnimal(string $name): Animal;
}
