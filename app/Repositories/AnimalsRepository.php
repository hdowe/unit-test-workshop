<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Models\Animal;
use Illuminate\Database\ConnectionInterface;

class AnimalsRepository implements AnimalsRepositoryInterface
{
    private ConnectionInterface $db;

    public function __construct(ConnectionInterface $db)
    {
        $this->db = $db;
    }

    public function createAnimal(string $name): Animal
    {
        $this->db->table('animals')
            ->insert([
                'name' => $name,
            ]);

        $animal = new Animal();
        $animal->setId((int) $this->db->getDoctrineConnection()->lastInsertId());
        $animal->setName($name);

        return $animal;
    }
}
