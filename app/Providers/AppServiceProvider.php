<?php

namespace App\Providers;

use App\Repositories\AnimalsRepository;
use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Application;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(AnimalsRepository::class, static function(Application $app): AnimalsRepository {
            return new AnimalsRepository($app->make('db')->connection('mysql'));
        });
    }
}
