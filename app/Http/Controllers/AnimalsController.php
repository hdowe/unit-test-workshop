<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Repositories\AnimalsRepository;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AnimalsController extends Controller
{
    private AnimalsRepository $repository;

    public function __construct(AnimalsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(Request $request): JsonResponse
    {
        $name = $request->json('name');

        $animal = $this->repository->createAnimal($name);

        return new JsonResponse([
            'id' => $animal->getId(),
            'name' => $animal->getName(),
        ], Response::HTTP_CREATED);
    }
}
