build:
	docker-compose build

unit-test:
	docker-compose exec --workdir=/var/www/html web php vendor/bin/phpunit

migrate:
	docker-compose exec --workdir=/var/www/html web php artisan migrate

install:
	docker-compose exec \
		--user=$(shell id -u):$(shell id -g) \
		web composer install --ignore-platform-reqs

	docker-compose exec web php artisan migrate:install
	docker-compose exec web php artisan migrate
